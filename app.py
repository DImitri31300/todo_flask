from flask import Flask, render_template, request, redirect, jsonify, url_for
from flask_pymongo import pymongo
from bson.objectid import ObjectId
# from flask_cors import CORS


app = Flask(__name__)
# mongo = pymongo(app)

CONNECTION_STRING = "mongodb+srv://root:root@cluster0-eakfn.mongodb.net/test?retryWrites=true&w=majority"
client = pymongo.MongoClient(CONNECTION_STRING)
db = client.get_database('todolist')
user_collection = pymongo.collection.Collection(db, 'msg')


@app.route('/')
def flask_mongodb_atlas():
    return render_template('index.html', name='name')

@app.route('/messages', methods=['POST', 'GET'])
def messages():
    messages = db.db.collection.messages.find({})
    return render_template('messages.html', messages=messages if messages.count() else False)

@app.route("/api/messages/save", methods=["POST"])
def api_messages_save():

    # message = {
    #     "message": request.form["message"],
    #     "done": False
    # }
    message = db.db.collection
    result = []
    for field in message.find():
        result.append({'_id': str(field['_id']), 'message': field['task']})
    print(result)
    db.db.collection.insert_one(message)
    return redirect(url_for("messages"))


@app.route('/task', methods=['GET'])
def task():
    task = db.db.collection
    result = []
    for field in task.find():
        result.append({'_id': str(field['_id']), 'name': field['name']})
    print(result)
    return render_template('task.html', result=result)
    return jsonify(result)

if __name__ == '__main__':
    app.run(debug=True)

